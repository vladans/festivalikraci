﻿using Festivali.Interfaces;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Festivali.Repository
{
    public class FestivalRepository : IDisposable, IFestivalRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public bool Add(Festival festival)
        {
            db.Festivali.Add(festival);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public bool Delete(Festival festival)
        {
            db.Festivali.Remove(festival);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public IEnumerable<Festival> GetAll()
        {
            return db.Festivali.Include(x => x.Mesto).OrderByDescending(x => x.Cena);
        }

        public Festival GetById(int id)
        {
            return db.Festivali.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Festival> GetPretraga(int start, int kraj)
        {
            return db.Festivali
                .Include(x=>x.Mesto)
                .Where(x => x.GodinaPrvogOdrzavanja > start && x.GodinaPrvogOdrzavanja < kraj)
                .OrderBy(x => x.GodinaPrvogOdrzavanja);
        }

        public bool Update(Festival festival)
        {
            db.Entry(festival).State = EntityState.Modified;
            try
            {
                int res = db.SaveChanges();
                return res > 0 ? true : false;
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}