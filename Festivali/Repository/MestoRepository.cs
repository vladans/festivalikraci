﻿using Festivali.Interfaces;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Festivali.Repository
{
    public class MestoRepository : IDisposable, IMestoRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public bool Add(Mesto mesto)
        {
            db.Mesta.Add(mesto);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public bool Delete(Mesto mesto)
        {
            db.Mesta.Remove(mesto);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public IEnumerable<Mesto> GetAll()
        {
            return db.Mesta;
        }

        public Mesto GetById(int id)
        {
            return db.Mesta.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Mesto> GetByCode(int code)
        {
            return db.Mesta.Where(x => x.PostanskiKod < code).OrderBy(x=>x.PostanskiKod);
        }

        public bool Update(Mesto mesto)
        {
            db.Entry(mesto).State = EntityState.Modified;

            try
            {
                int res = db.SaveChanges();
                return res > 0 ? true : false;
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}