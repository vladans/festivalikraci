namespace Festivali.Migrations
{
    using Festivali.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Festivali.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Festivali.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Mesta.AddOrUpdate(x => x.Id,
                new Mesto() { Id = 1, Naziv = "Budapest", PostanskiKod = 1025 },
                new Mesto() { Id = 2, Naziv = "Novi Sad", PostanskiKod = 21000 },
                new Mesto() { Id = 3, Naziv = "Budva", PostanskiKod = 85310 }
            );

            context.Festivali.AddOrUpdate(x => x.Id,
                new Festival() { Id = 1, Naziv = "Sziget", Cena = 150m, GodinaPrvogOdrzavanja = 1990, MestoId = 1 },
                new Festival() { Id = 2, Naziv = "Exit", Cena = 60m, GodinaPrvogOdrzavanja = 2000, MestoId = 2 },
                new Festival() { Id = 3, Naziv = "Sea dance", Cena = 30.5m, GodinaPrvogOdrzavanja = 2014, MestoId = 3 }
            );
        }
    }
}
