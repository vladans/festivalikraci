﻿using Festivali.Interfaces;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Festivali.Controllers
{
    //[Authorize]
    //[RoutePrefix("api/festivali")]
    public class FestivaliController : ApiController
    {
        IFestivalRepository _repository { get; set; }
        public FestivaliController(IFestivalRepository repository)
        {
            _repository = repository;
        }

        //[Authorize]
        //[Route("")]
        public IEnumerable<Festival> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var festival = _repository.GetById(id);
            if (festival == null)
            {
                return NotFound();
            }
            return Ok(festival);
        }

        [Authorize]
        public IHttpActionResult Post(Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(festival);
            return CreatedAtRoute("DefaultApi", new { id = festival.Id }, festival);
        }

        //[Route("Festivali/{customerId}/orders")]
        //[Route("{pretraga}")]
        //[HttpPut("api/festivali/pretraga")]      // Matches PUT 'api/festivali/pretraga'
        //[HttpPost("api/festivali/pretraga")] // Matches POST 'api/festivali/pretraga'
        //[AllowAnonymous]
        [Authorize]
        [Route("api/festivali/pretraga")]
        //[HttpPost]
        //[HttpPost, Route("api/festivali/pretraga")]
        //[ActionName("Pretragal")]
        //[ResponseType(typeof(Pretraga))]
        //public IEnumerable<Festival> PostPretraga([FromBody] Pretraga pretraga)   //[FromBody]
        public IHttpActionResult PostPretraga(Pretraga pretraga)    // [FromBody]
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var festivali = _repository.GetPretraga(pretraga.Start, pretraga.Kraj);
            if (festivali == null || !festivali.Any())
            {
                return NotFound();
            }
            return Ok(festivali);
        }

        public IHttpActionResult Put(int id, Festival festival)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != festival.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(festival);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(festival);
        }

        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            var festival = _repository.GetById(id);
            if (festival == null)
            {
                return NotFound();
            }

            _repository.Delete(festival);
            return Ok();
        }
    }
}
