﻿using Festivali.Interfaces;
using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Festivali.Controllers
{
    public class MestaController : ApiController
    {
        IMestoRepository _repository { get; set; }
        public MestaController(IMestoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Mesto> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var mesto = _repository.GetById(id);
            if (mesto == null)
            {
                return NotFound();
            }
            return Ok(mesto);
        }

        public IEnumerable<Mesto> GetMestaSaManjimKodom(int kod)
        {
            //var mesta = _repository.GetByCode(kod);
            //if (mesta == null)
            //{
            //    return NotFound();
            //}
            //return mesta;
            return _repository.GetByCode(kod);
        }

        public IHttpActionResult Post(Mesto mesto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(mesto);
            return CreatedAtRoute("DefaultApi", new { id = mesto.Id }, mesto);
        }

        public IHttpActionResult Put(int id, Mesto mesto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mesto.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(mesto);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(mesto);
        }

        public IHttpActionResult Delete(int id)
        {
            var mesto = _repository.GetById(id);
            if (mesto == null)
            {
                return NotFound();
            }

            _repository.Delete(mesto);
            return Ok();
        }
    }
}
