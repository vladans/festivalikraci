﻿$(document).ready(function () {

// PODACI OD INTERESA *******************************************************
    var host = 'http://' + window.location.host;
    var festivaliEndpoint = "/api/festivali/";
    var mestaEndpoint = "/api/mesta/";

    var token1 = sessionStorage.getItem(tokenKey);
    var headers1 = {};
    if (token1) {
        headers1.Authorization = 'Bearer ' + token1;
    }

    var token = null;
    var tokenType = null;
    var headers = {};
    var formAction = "Create";
    var editingId = "";

// PUNJENJE TABELE PODACIMA *******************************************************
    //populateTable();
    getFestivali();

// KLIK *******************************************************
    //$('#btnRegistracija').click(divRegistracija);
    $("body").on("click", "#btnRegistracija", divRegistracija);
    //$('#btnAdd').click(addItem);
    $("body").on("click", "#btnAdd", addItem);
    $("body").on("click", "#btnEditItem", editItem);
    $("body").on("click", "#btnDeleteItem", deleteItem);
    $("body").on("click", "#btnQuit", quit);
    //document.getElementById("btnRegistracija").onclick = divRegistracija;

// REGISTRACIJA KORISNIKA *******************************************************
    $("#formRegistracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();   // test@gmail.com
        var loz1 = $("#regLoz").val();      // Test@123
        var loz2 = $("#regLoz2").val();     // Test@123

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            //$("#prijavljen").append("Uspešna registracija. Možete se prijaviti na sistem.");
            document.getElementById("divRegistracija").classList.add("hidden");
            document.getElementById("divPrijava").classList.remove("hidden");

        }).fail(function (data) {
            alert("Doslo je do greske prilikom registracije.\n\n" + data.responseText);
        });
        document.getElementById("regEmail").value = "";
        document.getElementById("regLoz").value = "";
        document.getElementById("regLoz2").value = "";
    });

// PRIJAVA KORISNIKA *******************************************************
    $("#formPrijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": host + "/Token",
            "data": sendData

        }).done(function (data) {
            token = data.access_token;
            tokenType = data.token_type;
            document.getElementById("priEmail").value = "";
            document.getElementById("priLoz").value = "";
            document.getElementById("divPrijava").classList.add("hidden");
            document.getElementById("info").classList.remove("hidden");
            $("#prijavljen").empty().append(/*"Prijavljen korisnik: " +*/ data.userName);
            document.getElementById("pretraga").classList.remove("hidden");
            document.getElementById("dodavanje").classList.remove("hidden");

            let elements = document.getElementsByClassName("showHide");
            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.remove("hidden");
            }

            populateSelectList();

        }).fail(function (data) {
            document.getElementById("priEmail").value = "";
            document.getElementById("priLoz").value = "";
            alert("Doslo je do greske prilikom prijave.\n\n" + data.responseText);
        });
    });

// ODJAVA *******************************************************
    $("#odjava").click(function () {
        token = null;
        headers = {};
        formAction = "Create";
        editingId = "";

        $("#prijavljen").empty();
        document.getElementById("info").classList.add("hidden");
        document.getElementById("pretraga").classList.add("hidden");
        document.getElementById("dodavanje").classList.add("hidden");
        document.getElementById("divPrijava").classList.remove("hidden");

        let elements = document.getElementsByClassName("showHide");
        for (var i = 0; i < elements.length; i++) {
            elements[i].classList.add("hidden");
        }
    })

// PUNJENJE TABELE PODACIMA *******************************************************
    function populateTable(data) {
        $("#tableData1").empty();
        
        var body = document.getElementById('table1').getElementsByTagName('tbody')[0];
        for (var i = 0; i < data.length; i++) {
            // Insert a row in the table at the last row
            //var row1 = table1.insertRow(table1.rows.length);
            var row = body.insertRow(i);
            row.classList.add("text-center");

            var cell0 = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            var cell5 = row.insertCell(5);

            cell0.innerHTML = data[i].Naziv;
            cell1.innerHTML = data[i].Mesto.Naziv;
            cell2.innerHTML = data[i].GodinaPrvogOdrzavanja;
            cell3.innerHTML = data[i].Cena;

            cell4.classList.add("showHide");
            cell5.classList.add("showHide");
            if (!token) {
                cell4.classList.add("hidden");
                cell5.classList.add("hidden");
            }

            var btn1 = document.createElement('button');    //var btn1 = document.createElement('input');
            var btn2 = document.createElement('button');    //var btn2 = document.createElement('input');
            btn1.classList.add("btn", "btn-default");
            btn2.classList.add("btn", "btn-default");
            btn1.textContent = "Izmeni";
            btn2.textContent = "Obrisi";
            btn1.value = data[i].Id;
            btn2.value = data[i].Id;
            btn1.name = "id";
            btn2.name = "id";
            btn1.id = "btnEditItem";
            btn2.id = "btnDeleteItem";
            cell4.appendChild(btn1);
            cell5.appendChild(btn2);
        }
    }

// AJAX GET FESTIVALI *************************************************************
    function getFestivali() {
        $.ajax({
            type: "GET",
            url: host + festivaliEndpoint
        })
            .done(function (data, status) {
                populateTable(data);
            })
            .fail(function (data, status) {
                alert("Doslo je do greske prilikom dobavljanja stavki.\n\n" + data.responseText);
            });
    }

// CLICK SUBMIT PRETRAGA *******************************************************
    $("#formPretraga").submit(function (e) {
        e.preventDefault();

        if (token) {
            let data = getPretraga();
            document.getElementById("start").value = "";
            document.getElementById("kraj").value = "";
        }
        else {
            alert("Morate biti ulogovani.")
        }
    });

// AJAX GET PRETRAGA *************************************************************
    function getPretraga() {
        //// objekat koji se salje
        //let sendData = {};
        //sendData.Start = document.getElementById("start").value;
        //sendData.Kraj = document.getElementById("kraj").value;

        // objekat koji se salje
        let sendData = {
            "Start": document.getElementById("start").value,
            "Kraj": document.getElementById("kraj").value
        };

        $.ajax({
            type: "POST",
            url: host + festivaliEndpoint + "pretraga",
            data: sendData
        })
        .done(function (data, status) {
            populateTable(data);
        })
        .fail(function (data, status) {
            var jsonResponse = JSON.parse(data.responseText);
            var obj1 = jsonResponse.ModelState;
            var text = "";
            for (var key in obj1) {
                text += obj1[key] + ".\n";
            }
            alert("Doslo je do greske prilikom dobavljanja stavki.\n\n" + text);
        });
    }

// PUNJENJE SELECT LISTE *******************************************************
    function populateSelectList() {
        var sel = document.getElementById("sel1");
        //if (sel) {
        //    sel.options[sel.options.length] = new Option(name, i);
        //}
        //else {
        //    alert("Greska prilikom punjenja select liste.");
        //}
        $.ajax({
            url: host + mestaEndpoint,
            type: "GET",
        })
        .done(function (data, status) {
            if ($('#sel1 option').length == 0) {
                for (var i = 0; i < data.length; i++) {
                    //var option = document.createElement('option');
                    //option.value = data[i].Id;
                    //if (data[i].Id == 1) {
                    //    option.selected = true;
                    //}
                    //option.text = data[i].Naziv;
                    //sel.add(option, 0);
                    if (sel) {
                        sel.options[i] = new Option(data[i].Naziv, data[i].Id);
                    }
                }
            }
        })
        .fail(function (data, status) {
            alert("Desila se greska prilikom popunjavanja padajuceg menija.\n\n" + data.responseText);
        });
    }

// KLIK btnRegistracija *******************************************************
    function divRegistracija() {
        document.getElementById("divPrijava").classList.add("hidden");
        document.getElementById("divRegistracija").classList.remove("hidden");
    }

// BRISANJE STAVKE ************************************************************
    function deleteItem() {
        var deleteId = this.value;
        if (token) {
            $.ajax({
                url: host + festivaliEndpoint + deleteId,
                type: "DELETE",
            })
            .done(function (data, status) {
                 //populateTable(data);
                getFestivali();
            })
            .fail(function (data, status) {
                alert("Desila se greska prilikom brisanja stavke.\n\n" + data.responseText);
            });
        }
        else {
            alert("Morate biti ulogovani da bi ste obrisali stavku.\n\n" + data.responseText)
        }
    };

// DODAVANJE STAVKE ************************************************************
    function addItem() {
        if (token) {
            $("#addEditForm").submit(function (e) {
                e.preventDefault();

                var itemName = document.getElementById("itemName").value;
                var itemPrice = document.getElementById("itemPrice").value;
                var itemDate = document.getElementById("itemDate").value;
                var itemSelected = document.getElementById("sel1").value;
                //var url = "";
                //var httpMethod = "";
                //var sendData = {};

                // objekat koji se salje
                sendData = {
                    "Naziv": itemName,
                    "Cena": itemPrice,
                    "GodinaPrvogOdrzavanja": itemDate,
                    "MestoId": itemSelected
                };

                if (formAction === "Create") {
                    url = host + festivaliEndpoint;
                    httpMethod = "POST";
                }
                else {
                    httpMethod = "PUT";
                    url = host + festivaliEndpoint + editingId;
                    sendData.Id = editingId;
                    //sendData = {
                    //    "Id": editId,
                    //    "Name": itemName,
                    //    "Price": itemPrice,
                    //    "Date": itemDate
                    //};
                    formAction = "Create";
                }

                $.ajax({
                    url: url,
                    type: httpMethod,
                    data: sendData
                })
                .done(function (data, status) {
                    document.getElementById("itemName").value = "";
                    document.getElementById("itemPrice").value = "";
                    document.getElementById("itemDate").value = "";
                    document.getElementById("sel1").value = 1;
                    //populateTable(data);
                    getFestivali();
                })
                .fail(function (data, status) {
                    if (formAction === "Create") {
                        var jsonResponse = JSON.parse(data.responseText);
                        var obj1 = jsonResponse.ModelState;
                        var text = "";
                        for (var key in obj1) {
                            text += obj1[key] + ".\n";
                        }
                        alert("Desila se greska prilikom dodavanja nove stavke.\n\n" + text);
                    }
                    else {
                        alert("Desila se greska prilikom editovanja stavke.\n\n" + data.responseText);
                    }
                })
                editingId = "";
            });
        } else {
            alert("You must be loged in to add item.")
        }
    }

// KLIK btnQuit *******************************************************
    function quit() {
        document.getElementById("itemName").value = "";
        document.getElementById("itemPrice").value = "";
        document.getElementById("itemDate").value = "";
        document.getElementById("sel1").value = 1;
    }

// EDITOVANJE STAVKE ************************************************************
    function editItem() {
        var editId = this.value;
        if (token) {
            headers.Authorization = 'Bearer ' + token;

            // saljemo zahtev da dobavimo stavku
            $.ajax({
                url: host + festivaliEndpoint + editId,
                type: "GET",
                "headers": headers
            })
                .done(function (data, status) {
                    //$("#itemName").val(data.Naziv);
                    document.getElementById("itemName").value = data.Naziv;
                    //$("#itemPrice").val(data.Cena);
                    document.getElementById("itemPrice").value = data.Cena;
                    //$("#itemDate").val(data.GodinaPrvogOdrzavanja);
                    document.getElementById("itemDate").value = data.GodinaPrvogOdrzavanja;
                    document.getElementById("sel1").value = data.MestoId;
                    editingId = data.Id;
                    formAction = "Update";
                })
                .fail(function (data, status) {
                    formAction = "Create";
                    alert("Desila se greska prilikom editovanja stavke.");
                });
        } else {
            alert("You must be loged in to add item.")
        }
    }

// UCITAVANJE PRVOG PROIZVODA *******************************************************
    //$("#proizvodi").click(function () {
    //    // korisnik mora biti ulogovan
    //    if (token) {
    //        headers.Authorization = 'Bearer ' + token;
    //    }

    //    $.ajax({
    //        "type": "GET",
    //        "url": host + "/api/products/1",
    //        "headers": headers

    //    }).done(function (data) {
    //        $("#sadrzaj").append("Proizvod: " + data.Name);

    //    }).fail(function (data) {
    //        alert(data.status + ": " + data.statusText);  //data.responseText
    //    });
    //});

//// PUNJENJE TABELE PODACIMA *******************************************************
//    function populateTable() {
//        $("#tableData1").empty();
//        $.ajax({
//            type: "GET",
//            url: host + festivaliEndpoint
//        })
//        .done(function (data, status) {
//            var body = document.getElementById('table1').getElementsByTagName('tbody')[0];
//            for (var i = 0; i < data.length; i++) {
//                // Insert a row in the table at the last row
//                //var row1 = table1.insertRow(table1.rows.length);
//                var row = body.insertRow(i);
//                row.classList.add("text-center");

//                var cell0 = row.insertCell(0);
//                var cell1 = row.insertCell(1);
//                var cell2 = row.insertCell(2);
//                var cell3 = row.insertCell(3);
//                var cell4 = row.insertCell(4);
//                var cell5 = row.insertCell(5);

//                cell0.innerHTML = data[i].Naziv;
//                cell1.innerHTML = data[i].Mesto.Naziv;
//                cell2.innerHTML = data[i].GodinaPrvogOdrzavanja;
//                cell3.innerHTML = data[i].Cena;

//                cell4.classList.add("showHide");
//                cell5.classList.add("showHide");
//                if (!token) {
//                    cell4.classList.add("hidden");
//                    cell5.classList.add("hidden");
//                }

//                var btn1 = document.createElement('button');    //var btn1 = document.createElement('input');
//                var btn2 = document.createElement('button');    //var btn2 = document.createElement('input');
//                btn1.classList.add("btn", "btn-default");
//                btn2.classList.add("btn", "btn-default");
//                btn1.textContent = "Izmeni";
//                btn2.textContent = "Obrisi";
//                btn1.value = data[i].Id;
//                btn2.value = data[i].Id;
//                btn1.name = "id";
//                btn2.name = "id";
//                btn1.id = "btnEditItem";
//                btn2.id = "btnDeleteItem";
//                cell4.appendChild(btn1);
//                cell5.appendChild(btn2);
//            }
//        })
//        .fail(function (data, status) {
//            alert("Doslo je do greske prilikom popunjavanja tabele.\n\n" + data.responseText);
//        });
//    }
});