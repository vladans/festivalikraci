﻿using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Festivali.Interfaces
{
    public interface IMestoRepository
    {
        IEnumerable<Mesto> GetAll();
        Mesto GetById(int id);
        IEnumerable<Mesto> GetByCode(int code);
        bool Add(Mesto mesto);
        bool Update(Mesto mesto);
        bool Delete(Mesto mesto);
    }
}
