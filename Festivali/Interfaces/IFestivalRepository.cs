﻿using Festivali.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Festivali.Interfaces
{
    public interface IFestivalRepository
    {
        IEnumerable<Festival> GetAll();
        Festival GetById(int id);
        IEnumerable<Festival> GetPretraga(int start, int kraj);
        bool Add(Festival festival);
        bool Update(Festival festival);
        bool Delete(Festival festival);
    }
}
