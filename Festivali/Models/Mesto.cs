﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Festivali.Models
{
    public class Mesto
    {
        public int Id { get; set; }
        public string Naziv { get; set; }

        [Required]
        [Range(0, 99999, ErrorMessage = "Vrednost ne sme preci 5 cifara.")]
        public int PostanskiKod { get; set; }
    }
}