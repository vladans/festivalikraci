﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Festivali.Models
{
    public class Pretraga
    {
        [Required]
        public int Start { get; set; }
        [Required]
        public int Kraj { get; set; }

    }
}