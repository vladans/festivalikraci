﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Festivali.Models
{
    public class Festival
    {
        public int Id { get; set; }
        public string Naziv { get; set; }

        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage = "Vrednost mora biti veca od 0")]
        public decimal Cena { get; set; }

        [Required]
        [Range(1951, 2017, ErrorMessage = "Godina mora biti veca od 1950 a manja od 2018.")]
        public int GodinaPrvogOdrzavanja { get; set; }

        public int MestoId { get; set; }
        //[ForeignKey("MestoId")]
        public Mesto Mesto { get; set; }

    }
}