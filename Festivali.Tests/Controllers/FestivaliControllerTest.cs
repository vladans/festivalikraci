﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Festivali.Controllers;
using Festivali.Interfaces;
using Festivali.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Festivali.Tests.Controllers
{
    [TestClass]
    public class FestivaliControllerTest
    {
        [TestMethod]
        public void GetReturnsFestivalWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IFestivalRepository>();
            mockRepository.Setup(x => x.GetById(50)).Returns(new Festival { Id = 50, Naziv = "Test", Cena = 99m, GodinaPrvogOdrzavanja = 1999, MestoId = 1 });

            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(50);
            var contentResult = actionResult as OkNegotiatedContentResult<Festival>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(50, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IFestivalRepository>();
            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(51);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<IFestivalRepository>();
            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(52);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IFestivalRepository>();
            mockRepository.Setup(x => x.GetById(53)).Returns(new Festival { Id = 53, Naziv = "Test", Cena = 99m, GodinaPrvogOdrzavanja = 1999, MestoId = 1 });
            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(53);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IFestivalRepository>();
            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(54, new Festival { Id = 100, Naziv = "Test", Cena = 99m, GodinaPrvogOdrzavanja = 1999, MestoId = 1 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        // public void PostMethodReturns201WithObject()
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IFestivalRepository>();
            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Festival { Id = 55, Naziv = "Test55", Cena = 99m, GodinaPrvogOdrzavanja = 1999, MestoId = 1 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Festival>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(55, createdResult.RouteValues["id"]);

            //DODATAK
            Assert.IsNotNull(createdResult.Content);
            Assert.AreEqual(55, createdResult.Content.Id);
            Assert.AreEqual("Test55", createdResult.Content.Naziv);
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Festival> festivali = new List<Festival>();
            festivali.Add(new Festival { Id = 56, Naziv = "Test", Cena = 99m, GodinaPrvogOdrzavanja = 1999, MestoId = 1 });
            festivali.Add(new Festival { Id = 57, Naziv = "Test", Cena = 99m, GodinaPrvogOdrzavanja = 1999, MestoId = 1 });

            var mockRepository = new Mock<IFestivalRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(festivali.AsEnumerable());
            var controller = new FestivaliController(mockRepository.Object);

            // Act
            IEnumerable<Festival> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(festivali.Count, result.ToList().Count);
            Assert.AreEqual(festivali.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(festivali.ElementAt(1), result.ElementAt(1));
        }
    }
}
